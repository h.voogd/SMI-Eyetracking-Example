## SMI iViewX Eyetracker Example SCE file
#
# Example made by Hubert F.J.M. Voogd  (h.voogd@socsci.ru.nl)

scenario = "SMI-Eyetracker-Example";
pcl_file = "SMI-Eyetracker-ExamplePCL.pcl";

active_buttons = 4;
button_codes = 1,2,3,4;

$width_screen = 1920;		# width of screen. The default SMI screen has a width of 1680. In our labs, the default is 1920.
$height_screen = 1080;		# height of screen. The default SMI screen has a height of 1050. In our labs, the default is 1080.

begin;


picture {

} p_Default; ## black screen


#calibration bitmap
picture
{
	background_color = 128, 128, 128; # background color should more or less match with the mean luminance of the screen during the experiment.
   bitmap 
	{
		filename = "white circle.bmp"; 
		trans_src_color = 0,0,0; 
	};
   x = 0; 
	y = 0;	
	
} et_calibration;

picture { text { caption =
"A white circle with a black dot at the center will appear on screen.
Please look at the black dot in the center of the white circle.\n
[ Press Y to start ]"; font_size = 20;}
t_Calibration; x = 0; y = 0; } p_Calibration;	##calibration instructions


picture { text { caption = 
"Validation Results: \n\n Accuracy X: - \n Accuracy Y: -\n \n"; } t_Accuracy;  x = 0; y = 0; } p_Accuracy;	## calibration accuracy picture


box { height = 1; width = 30; color = 255,255,255; } Horz;
box { height = 30; width = 1; color = 255,255,255; } Vert;


picture {
			box { height = 1; width = 30; color = 255,255,255; } b_Horz;x = 0; y = 0;
			box { height = 30; width = 1; color = 255,255,255; } b_Vert;x = 0; y = 0;
} p_Fixation; ## fixation cross


picture{
	text{caption = "Participant number?";font_size = 20;} t_Response1; x=0; y=0;
	text{caption = " ";font_size = 20;} t_Response2; x=0; y=-40;
}p_InfoOutput;


picture { text { caption = "Welcome!"; font_size = 20; } t_Begin1; x = 0; y = 70;
			 text { caption = "In this experiment two images are presented on screen."; font_size = 20; } t_Begin2; x = 0; y = 35;
			 text { caption = "The computer will calibrate the eyetracker first."; font_size = 20; } t_Begin4; x = 0; y = -140;
			 text { caption = "[ Press ENTER to start the experiment ]"; font_size = 20; } t_Begin5; x = 0; y = -210;
} p_Begin; ## welcome screen


picture { text { caption = "This is the end of this task."; font_size = 20; } t_End1; x = 0; y = 0;
			 text { caption = "Thanks for your participation!"; font_size = 20; } t_End2; x = 0; y = -35;
} p_End; ## end screen


picture { 
			text { caption = "Press ENTER to continue to the next block."; font_size = 20; } t_Text1; x = 0; y = 0;	
} p_Text;


picture{
  bitmap{filename = "Penguins.jpg";  width = 650; height = 400;} PictureLeft; x = -420; y = 0; 
  bitmap{filename = "Koala.jpg";   width = 650; height = 400;} PictureRight;x = 420; y = 0;
} p_Stimulus;